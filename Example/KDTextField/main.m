//
//  main.m
//  KDTextField
//
//  Created by Kedar Desai on 12/02/2016.
//  Copyright (c) 2016 Kedar Desai. All rights reserved.
//

@import UIKit;
#import "KDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KDAppDelegate class]));
    }
}
