# KDTextField

[![CI Status](http://img.shields.io/travis/Kedar Desai/KDTextField.svg?style=flat)](https://travis-ci.org/Kedar Desai/KDTextField)
[![Version](https://img.shields.io/cocoapods/v/KDTextField.svg?style=flat)](http://cocoapods.org/pods/KDTextField)
[![License](https://img.shields.io/cocoapods/l/KDTextField.svg?style=flat)](http://cocoapods.org/pods/KDTextField)
[![Platform](https://img.shields.io/cocoapods/p/KDTextField.svg?style=flat)](http://cocoapods.org/pods/KDTextField)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KDTextField is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KDTextField"
```

## Author

Kedar Desai, kedar.dwl@gmail.com

## License

KDTextField is available under the MIT license. See the LICENSE file for more info.
